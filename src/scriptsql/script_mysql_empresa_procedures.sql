--
-- Clientes que han hecho compras los ultimos X dias
--
DELIMITER |
CREATE PROCEDURE compras_clientes(IN dias INT, OUT num int)
BEGIN
SELECT count(*) INTO num FROM clientes WHERE Id IN (        SELECT cliente FROM facturas WHERE fecha>=DATE_SUB(CURDATE(),INTERVAL dias DAY));
END |


--
-- Total factura
--

CREATE PROCEDURE total_factura(IN fact INT(10), OUT total float)
BEGIN
select sum(importe) into total from lineas_factura where factura = fact;
END |


--
-- Ventas por vendedor
--

CREATE VIEW ventas_vendedor AS 
SELECT * FROM vendedores
         WHERE NOT EXISTS (
                   SELECT 1 FROM Grupos 
                   WHERE NOT EXISTS (
                             SELECT  1 FROM Articulos 
                             WHERE Grupo=Grupos.Id 
                             AND EXISTS (
                                 SELECT 1 FROM lineas_factura, facturas
                                 WHERE lineas_factura.factura = facturas.id and Articulo=Articulos.Id 
                                 AND  vendedor=vendedores.Id 
                                 AND lineas_factura.Fecha>DATE_SUB(CURDATE(), 
                                              INTERVAL 365 DAY))))|
                                              
                                              
