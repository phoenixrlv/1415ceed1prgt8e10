/*
 * Main.java
 *
 * Created on 11 de agosto de 2008, 16:20
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo8;

/**
 *
 * @author alberto
 */
import java.sql.*;
    
public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
        public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = null;
        Statement stmt = null;
        
        try {
            //Registrando el Driver
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            System.out.println("Driver "+driver+" Registrado correctamente");
            
            //Abrir la conexi�n con la Base de Datos
            System.out.println("Conectando con la Base de datos...");
            String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";
            conn = DriverManager.getConnection(jdbcUrl,"paco","");
            System.out.println("Conexi�n establecida con la Base de datos...");
            
            //Uso del m�todo executeQuery
            stmt = conn.createStatement();
            
            conn.setAutoCommit(false);
            
            String sql = "update vendedores set salario=salario*0.90";
            
            int filas_vendedores = stmt.executeUpdate(sql);
                       
            sql = "update vendedores set salario=salario*1.10";

            filas_vendedores = stmt.executeUpdate(sql);
            
            sql = "update articulos set precio=precio*1.05";

            int filas_articulos = stmt.executeUpdate(sql);
            
            System.out.println("Numero de filas de vendedores afectadas "+filas_vendedores);
            System.out.println("Numero de filas de art�culos afectadas "+filas_articulos);
            
            conn.commit();
            
            
        } catch(SQLException se) {
            //Errores de JDBC
            se.printStackTrace();
        } catch(Exception e) {
            //Errores de Class.forName
            e.printStackTrace();
        } finally {
            try {
                System.out.println("Haciendo rollback!");
                conn.rollback();

                //conn.setAutoCommit(true);
                
                
                if(stmt!=null)
                    stmt.close();

                if(conn!=null)
                    conn.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try  
    }
    
}
