/*
 * Main.java
 *
 * Created on 31 de agosto de 2008, 0:51
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo25;

import java.sql.*;

/**
 *
 * @author pacoaldarias
 */
public class Main {

  /** Creates a new instance of Main */
  public Main() {
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) throws 
          InstantiationException, 
          ClassNotFoundException, 
          IllegalAccessException, 
          SQLException {
    // TODO code application logic here
    

    String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";
    String driver = "com.mysql.jdbc.Driver";
    Class.forName(driver).newInstance();
    Connection con = DriverManager.getConnection(jdbcUrl,"paco","");
    
    
    Connection conn_1 = null;
    Statement stmt_1 = null;
    Connection conn_2 = null;
    Statement stmt_2 = null;

    ResultSet rs = null;

    try {
      //Abrimos una conexi�n y creamos un statement object

      stmt_1 = con.createStatement();
      System.out.println("Conexion 1 creada... [CON]");
      //Contruimos la sentencia SQL
      rs = stmt_1.executeQuery("select count(*) from vendedores");
      rs.next();
      System.out.println("[CON1] El numero de vendedores es: " + rs.getInt(1));
      //rs.close();

      //stmt_1 = con.createStatement();
      System.out.println("Conexion 2 no creada... [CON]");
      //Contruimos la sentencia SQL
      rs = stmt_1.executeQuery("select count(*) from articulos");
      rs.next();
      System.out.println("[CON2] El numero de articulos es: " + rs.getInt(1));
      rs.close();

    } catch (SQLException se) {
    } catch (Exception e) {
    } finally {
      try {
        if (stmt_1 != null) {
          stmt_1.close();
        }
        if (con != null) {
          con.close();
        }
        if (stmt_2 != null) {
          stmt_2.close();
        }
      } catch (Exception fe) {
      }
    }

  }
}
