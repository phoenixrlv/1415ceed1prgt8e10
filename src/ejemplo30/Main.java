/*
 * Main.java
 *
 * Created on 9 de septiembre de 2008, 10:42
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo30;
import com.sun.rowset.CachedRowSetImpl;
import javax.sql.rowset.CachedRowSet;

/**
 *
 * @author alberto
 */
public class Main {
    
    //Constante que almacenar� la informaci�n del CachedRowSet
    private final static String CRS_FILE_LOC ="cachedrs.crs";
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        CachedRowSet crs = null;
        
        try {
            //Creamos el contexto inicial usando el sistema de ficheros
            crs = new CachedRowSetImpl();
            Class.forName("com.mysql.jdbc.Driver");
            crs.setUrl("jdbc:mysql://localhost:3306/empresa?relaxAutoCommit=true");
            crs.setUsername("root");
            crs.setPassword("");
            
            
            //Ejecutamos la sentencia
            String sql = "select * from articulos";
            crs.setCommand(sql);
            crs.execute();
            
            //Desplaza el cursor a la posici�n de inserci�n
            crs.moveToInsertRow();
            //A�ade los datos en la nueva fila
            crs.updateInt(1,11);
            crs.updateString(2,"SAI");
            crs.updateDouble(3,150.00);
            crs.updateString(4,"SAI");
            crs.updateInt(5,2);
            
            //Escribe las filas en el RowSet
            crs.insertRow();
            crs.moveToCurrentRow();
            
            //Env�a los cambios a la fuente de datos
            crs.acceptChanges();
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if ( crs != null ) {
                    crs.close();
                }
            } catch (Exception fe) {
                fe.printStackTrace();
            }
        }
    }
    
}
