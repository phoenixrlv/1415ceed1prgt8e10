/*
 * Main.java
 *
 * Created on 7 de septiembre de 2008, 11:51
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo27;
import com.sun.rowset.JdbcRowSetImpl;
import java.sql.*;


/**
 *
 * @author alberto
 */
public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        JdbcRowSetImpl jrs = null;
        String user = "root";
        String pass = "";
        String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";

        try {
            //Cargamos el driver 
            Class.forName("com.mysql.jdbc.Driver");
            
            //Creamos el objeto JdbcRowSet e 
            //inicializamos las propiedades de conexión
            jrs = new JdbcRowSetImpl();
            jrs.setUrl(jdbcUrl);
            jrs.setUsername(user);
            jrs.setPassword(pass);

            //Definimos la query
            String sql = "select * from vendedores where id = ?";
            
            //Inicializamos la query en el rowset
            jrs.setCommand(sql);
            
            //Damos valor al parámetro id
            jrs.setInt(1, 1);

            jrs.execute();
            
            //Mostramos los resultados
            while(jrs.next()){
                System.out.print("ID: " + jrs.getInt(1));
                System.out.print(", Nombre: " + jrs.getString(2));
                System.out.println();
            }

        } catch (SQLException se ){
            se.printStackTrace();
        } catch (Exception e ){
            e.printStackTrace();
        } finally {
            try {
                if (jrs != null ) {
                    jrs.close();
                }
                
            } catch (Exception fe) {
                fe.printStackTrace();
            }
        }
    }
    
}
