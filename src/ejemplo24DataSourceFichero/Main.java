/*
 * Main.java
 *
 * Created on 29 de agosto de 2008, 23:29
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package ejemplo24DataSourceFichero;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.sql.*;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author alberto
 */
public class Main {

  /**
   * Creates a new instance of Main
   */
  public Main() {
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    // TODO code application logic here
    Connection conn = null;
    Context ctx = null;
    String dataSourceName = "jdbc/empresa";
    Statement stmt = null;
    ResultSet rs = null;

    try {
      //Creamos el contexto inicial usando el sistema de ficheros
      System.out.println("Creamos el contexto Inicial...");
      Hashtable env = new Hashtable();
      String sp = "com.sun.jndi.fscontext.RefFSContextFactory";

      /* Crear la carpeta  /home/paco/jndi/jdbc
       y darle permisos de escritura */

      /* Requiere la libreria fscontext.jar 
       * que estan en la carpeta bibliotecas 
       * que cuelga de la carpeta raiz del
       * proyecto
       */


      String urlProvider = "file:/home/paco/jndi";
      env.put(Context.INITIAL_CONTEXT_FACTORY, sp);
      env.put(Context.PROVIDER_URL, urlProvider);
      ctx = new InitialContext(env);

      //Registramos el DataSource
      registrarDataSource(ctx, dataSourceName);


      //Buscamos el DataSource
      System.out.println("Buscamos el DataSource...");
      DataSource ds = null;
      ds = (DataSource) ctx.lookup(dataSourceName);

      //Abrimos la conexion buscando
      System.out.println("Abrimos la conexi�n...");
      conn = ds.getConnection();
      System.out.println("Conexi�n creada " + conn);

      stmt = conn.createStatement();
      String sql = "select * from vendedores";
      rs = stmt.executeQuery(sql);

      System.out.println("Listando el nombre de los empleados");

      while (rs.next()) {
        System.out.println(rs.getString("nombre"));
      }

    } catch (SQLException se) {
      //Errores de JDBC
      se.printStackTrace();
    } catch (Exception e) {
      //Errores de Class.forName
      e.printStackTrace();
    } finally {
      try {
        if (ctx != null) {
          ctx.close();
        }
        if (rs != null) {
          rs.close();
        }
        if (stmt != null) {
          stmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (NamingException ne) {
        ne.printStackTrace();
      } catch (SQLException se) {
        se.printStackTrace();
      }//end finally try
    }//end try  

  }

  public static void registrarDataSource(Context ctx,
          String dataSourceName)
          throws NamingException {
    //Instanciamos el objeto DataSource
    MysqlDataSource mds = new MysqlDataSource();
    mds.setUser("paco");
    mds.setPassword("");
    mds.setDatabaseName("empresa");
    mds.setPortNumber(3306);
    mds.setServerName("localhost");

    //Registramos el objeto
    ctx.rebind(dataSourceName, mds);

  }
}
