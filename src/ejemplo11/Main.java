/*
 * Main.java
 *
 * Created on 12 de agosto de 2008, 18:23
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package ejemplo11;

import java.sql.*;

/**
 *
 * @author alberto
 */
public class Main {

  /**
   * Creates a new instance of Main
   */
  public Main() {
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    // TODO code application logic here
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;

    try {
      //Registrando el Driver
      String driver = "com.mysql.jdbc.Driver";
      Class.forName(driver).newInstance();
      System.out.println("Driver " + driver + " Registrado correctamente");

      //Abrir la conexi�n con la Base de Datos
      System.out.println("Conectando con la Base de datos...");
      String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";
      conn = DriverManager.getConnection(jdbcUrl, "paco", "");
      System.out.println("Conexi�n establecida con la Base de datos...");

      stmt = conn.createStatement();

      //Ejecutamos la SELECT sobre la tabla articulos
      String sql = "select * from articulos";

      rs = stmt.executeQuery(sql);

      System.out.println("Cursor antes de la primera fila = " + rs.isBeforeFirst());
      int id;
      String nombre;
      java.math.BigDecimal precio;
      String codigo;
      int grupo;

      while (rs.next()) {
        //Obtenemos la informaci�n por el nombre de la columna

        id = rs.getInt("id");
        nombre = rs.getString("nombre");
        precio = rs.getBigDecimal("precio");

        //Obtenemos la informaci�n por el indice de la columna
        codigo = rs.getString(4);
        grupo = rs.getInt(5);


        //Mostramos la informaci�n
        System.out.print("Numero de Fila=" + rs.getRow());
        System.out.print(", id: " + id);
        System.out.print(", nombre: " + nombre);
        System.out.print(", precio: " + precio.floatValue() + "�");
        System.out.print(", codigo: " + codigo);
        System.out.println(", grupo: " + grupo);
      }
      System.out.println("Cursor despues de la ultima fila= " + rs.isAfterLast());
    } catch (SQLException se) {
      //Errores de JDBC
      se.printStackTrace();
    } catch (Exception e) {
      //Errores de Class.forName
      e.printStackTrace();
    } finally {
      try {
        if (rs != null) {
          rs.close();
        }
        if (stmt != null) {
          stmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException se) {
        se.printStackTrace();
      }//end finally try
    }//end try
  }
}
