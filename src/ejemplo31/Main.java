/*
 * Main.java
 *
 * Created on 9 de septiembre de 2006, 17:58
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo31;

import com.sun.rowset.WebRowSetImpl;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import javax.sql.rowset.WebRowSet;

/**
 *
 * @author pacoaldarias
 */
public class Main {
  //Constante que representa el fichero XML 

  private static String WRS_FILE_LOC = "wrs.xml";

  /** Creates a new instance of Main */
  public Main() {
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    // TODO code application logic here
    try {
      //Instanciamos el objeto WebRowSet 
      WebRowSet wrs = new WebRowSetImpl();

      //Cargamos el driver y parametrizamos la conexi�n
      Class.forName("com.mysql.jdbc.Driver");
      wrs.setUrl("jdbc:mysql://localhost:3306/empresa");
      wrs.setUsername("paco");
      wrs.setPassword("");


      //Escribimos el fichero XML
      writeXmlFile(wrs);

      //Leemos el fichero XML y obtenemos el WebRowSet
      WebRowSet wrs2 = readXmlFile();

      //Mostramos los valores
      while (wrs2.next()) {
        System.out.print("ID = " + wrs2.getInt(1));
        System.out.print(", Nombre = " + wrs2.getString(2));
        System.out.print(", Direccion = " + wrs2.getString(3));
        System.out.println();

      }
      wrs2.close();
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public static void writeXmlFile(WebRowSet wrs) throws SQLException, IOException {
    //Configuramos la sentencia
    System.out.println("Conectando a la fuente de datos");
    String sql = "select * from clientes";
    wrs.setCommand(sql);
    wrs.execute();

    //Escribimos el fichero XML 
    System.out.println("Escribiendo fichero XML: " + WRS_FILE_LOC);
    FileWriter fw = new FileWriter(WRS_FILE_LOC);
    wrs.writeXml(fw);
    fw.close();
    wrs.close();
    System.out.println("Finalizado el proceso de escritura en fichero.");
  }

  public static WebRowSet readXmlFile() throws SQLException, IOException {
    WebRowSet wrs2 = new WebRowSetImpl();
    //Leemos el fichero
    FileReader fr = new FileReader(WRS_FILE_LOC);
    //Rellenamos el WebRowSet a partir del fichero XML
    wrs2.readXml(fr);
    return wrs2;
  }
}
