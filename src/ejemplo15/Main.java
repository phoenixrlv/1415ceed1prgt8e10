/*
 * Main.java
 *
 * Created on 23 de agosto de 2008, 11:28
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo15;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
/**
 *
 * @author alberto
 */
public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
     public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = null;
        PreparedStatement pstmt = null;
        
        try {
            //Registrando el Driver
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            
            System.out.println("Driver "+driver+" Registrado correctamente");
            
            //Abrir la conexi�n con la Base de Datos
            System.out.println("Conectando con la Base de datos...");
            String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";
            conn = DriverManager.getConnection(jdbcUrl,"root","");
            System.out.println("Conexi�n establecida con la Base de datos...");
            
            //Cramos el objeto PreparedStatement
            pstmt = conn.prepareStatement("update facturas set serie=? where id=?");            
            
            //A�adimos los parametros del PreparedStatement
            pstmt.setString(1, "B");
            pstmt.setLong(2, 1);
            
            //Ejecutamos el prepared Statement
            pstmt.executeUpdate();
            System.out.println("Modificada la factura!");
            
        } catch(SQLException se) {
            //Errores de JDBC
            se.printStackTrace();
        } catch(Exception e) {
            //Errores de Class.forName
            e.printStackTrace();
        } finally {
            try {
                if(pstmt!=null)
                    pstmt.close();
                if(conn!=null)
                    conn.close();
                System.out.println("Cerrando conexi�n con la BD");
            } catch(SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try  
    }
    
}
