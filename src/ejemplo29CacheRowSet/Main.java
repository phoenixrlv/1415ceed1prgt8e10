/*
 * Main.java
 *
 * Created on 8 de septiembre de 2008, 0:23
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo29CacheRowSet;

import com.sun.rowset.CachedRowSetImpl;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.SQLException;

/**
 *
 * @author pacoaldarias
 */
public class Main {

  //Constante que almacenar� la informaci�n del CachedRowSet
  private final static String CRS_FILE_LOC = "cachedrs.crs";

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    // TODO code application logic here
    try {
      //Creamos un objeto CacheRowSetImpl serializado en un fichero
      writeCachedRowSet();

      //Creamos el  CachedRowSetImpl a partir del objeto serializado
      CachedRowSetImpl crs = readCachedRowSet();

      if (crs != null) {

        //Mostramos el resultado
        while (crs.next()) {
          System.out.print("ID: " + crs.getInt("id"));
          System.out.print(", Nombre: " + crs.getString("nombre"));
          System.out.print(", Precio: �" + crs.getDouble("precio"));
          System.out.print(", Codigo: " + crs.getString("codigo"));
          System.out.print(", Grupo: " + crs.getInt("grupo"));

          System.out.println();
        }
        //Cerramos el RowSet
        crs.close();
      } else {
        System.out.println("CachedRowSet es null");
      }
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public static void writeCachedRowSet() throws Exception {
    //Instanciamos un objeto CachedRowSetImpl y parametrizamos la conexi�n
    CachedRowSetImpl crs = new CachedRowSetImpl();
    Class.forName("com.mysql.jdbc.Driver");
    crs.setUrl("jdbc:mysql://localhost:3306/empresa");
    crs.setUsername("paco");
    crs.setPassword("");

    //Ejecutamos la sentencia
    String sql = "select * from articulos";
    crs.setCommand(sql);
    crs.execute();

    //Serializamos el objeto CachedRowSetImpl.
    FileOutputStream fos = new FileOutputStream(CRS_FILE_LOC);
    ObjectOutputStream out = new ObjectOutputStream(fos);
    out.writeObject(crs);
    out.close();
    crs.close();
  }

  public static CachedRowSetImpl readCachedRowSet() throws Exception {
    //Leemos el objeto CachedRowSetImpl serializado desde el almac�n
    FileInputStream fis = new FileInputStream(CRS_FILE_LOC);
    ObjectInputStream in = new ObjectInputStream(fis);
    CachedRowSetImpl crs = (CachedRowSetImpl) in.readObject();
    fis.close();
    in.close();
    return crs;
  }
}
