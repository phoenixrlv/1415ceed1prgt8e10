/*
 * Main.java
 *
 * Created on 7 de septiembre de 2008, 10:20
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo26Pool;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import java.sql.*;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


/**
 *
 * @author alberto
 */
public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = null;
        Context ctx = null;
        String poolName = "jdbc/pool";
        
        try {
            //Creamos el contexto inicial usando el sistema de ficheros
            System.out.println("Creamos el contexto Inicial...");
            Hashtable env = new Hashtable();
            
            String sp = Definir_el_repositorio_de_tipo_FS;
            String urlProvider = Definir_la_url_donde_se_encontrar�;
            Inicializar_el_repositorio;            
            
            //Registramos el DataSource
            System.out.println("Registramos el Pool de conexiones...");
            
            registrarPool(ctx, poolName);
            
            System.out.println("Pool registrado");
        } catch(SQLException se) {
            //Errores de JDBC
            se.printStackTrace();
        } catch(Exception e) {
            //Errores de Class.forName
            e.printStackTrace();
        } finally {
            try {
                if(ctx !=null)
                    ctx.close();                
            } catch(NamingException ne) {
                ne.printStackTrace();
            } 
        }//end try  

    }
    
    public static void registrarPool(Context ctx, String poolName) 
            throws SQLException, NamingException {
            //Instanciamos el objeto DataSource
            
            Instanciamos_el_objeto_MysqlConnectionPoolDataSource;
            Inicializamos_las_propiedades;
            Hacemos_el_registro_en_el_contexto;

    }
    
}
