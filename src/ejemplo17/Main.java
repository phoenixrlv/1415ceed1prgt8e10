/*
 * Main.java
 *
 * Created on 23 de agosto de 2008, 22:35
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo17;
import java.sql.*;

/**
 *
 * @author alberto
 */
public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
     public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = null;
        CallableStatement cstmt = null;
        
        try {
            //Registrando el Driver
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            System.out.println("Driver "+driver+" Registrado correctamente");
            
            //Abrir la conexi�n con la Base de Datos
            System.out.println("Conectando con la Base de datos...");
            String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";
            conn = DriverManager.getConnection(jdbcUrl,"root","");
            System.out.println("Conexi�n establecida con la Base de datos...");
            
            cstmt = conn.prepareCall("{call compras_clientes(?,?)}");
            cstmt.registerOutParameter(2, java.sql.Types.INTEGER);
            cstmt.setInt(1, 360);
            cstmt.execute();

            int dias = cstmt.getInt(2);
            
            System.out.println("El numero de clientes con compras de todo son: "+dias);
            
            
        } catch(SQLException se) {
            //Errores de JDBC
            se.printStackTrace();
        } catch(Exception e) {
            //Errores de Class.forName
            e.printStackTrace();
        } finally {
            try {
                if(cstmt!=null)
                    cstmt.close();
                if(conn!=null)
                    conn.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try  
    }
    
}
