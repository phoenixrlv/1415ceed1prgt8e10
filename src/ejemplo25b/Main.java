/*
 * Main.java
 *
 * Created on 31 de agosto de 2008, 0:51
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo25;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import java.sql.*;
import javax.sql.DataSource;
import javax.sql.PooledConnection;

/**
 *
 * @author pacoaldarias
 */
public class Main {

  /** Creates a new instance of Main */
  public Main() {
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    // TODO code application logic here
    MysqlConnectionPoolDataSource mcpds = null;
    PooledConnection pc = null;
    Connection conn_1 = null;
    Statement stmt_1 = null;
    Connection conn_2 = null;
    Statement stmt_2 = null;
    String user = "paco";
    String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";
    ResultSet rs = null;

    try {
      //Creamos el pool con MysqlConnectionPoolDataSource
      mcpds = new MysqlConnectionPoolDataSource();
      mcpds.setUser(user);
      mcpds.setPassword("");
      mcpds.setURL(jdbcUrl);



      //Obtenemos el PooledConnection
      pc = mcpds.getPooledConnection();


      //Abrimos una conexi�n y creamos un statement object

      conn_1 = pc.getConnection();

      stmt_1 = conn_1.createStatement();
      System.out.println("Conexion 1 creada... [CON1]");
      //Contruimos la sentencia SQL
      rs = stmt_1.executeQuery("select count(*) from vendedores");
      rs.next();
      System.out.println("[CON1] El numero de vendedores es: " + rs.getInt(1));
      rs.close();

      //Abrimos una nueva conexi�n y creamos un statement object
      conn_2 = pc.getConnection();

      stmt_2 = conn_2.createStatement();
      System.out.println("Conexion 2 creada... [CON2]");
      //Contruimos la sentencia SQL
      rs = stmt_2.executeQuery("select count(*) from articulos");
      rs.next();
      System.out.println("[CON2] El numero de articulos es: " + rs.getInt(1));
      rs.close();

    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (stmt_1 != null) {
          stmt_1.close();
        }
        if (conn_1 != null) {
          conn_1.close();
        }
        if (stmt_2 != null) {
          stmt_2.close();
        }
        if (conn_2 != null) {
          conn_2.close();
        }
        if (pc != null) {
          pc.close();
        }

      } catch (Exception fe) {
        fe.printStackTrace();
      }
    }

  }
}
