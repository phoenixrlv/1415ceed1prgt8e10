/*
 * Main.java
 *
 * Created on 24 de agosto de 2008, 11:14
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo20;
import java.sql.*;
/**
 *
 * @author alberto
 */
public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
     public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = null;
        DatabaseMetaData dbmd = null;
        
        try {
            //Registrando el Driver
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            System.out.println("Driver "+driver+" Registrado correctamente");
            
            //Abrir la conexi�n con la Base de Datos
            System.out.println("Conectando con la Base de datos...");
            String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";
            conn = DriverManager.getConnection(jdbcUrl,"root","");
            System.out.println("Conexi�n establecida con la Base de datos...");
            
            dbmd = conn.getMetaData();
            //Informaci�n del producto de la base de datos
            infoProducto(dbmd);
            //Informaci�n del driver JDBC
            infoDriver(dbmd);
            //Informaci�n sobre las funciones de la base de datos
            infoFunciones(dbmd);
            //Tablas existentes
            infoTablas(dbmd);
            //Procedimientos existentes
            infoProcedimientos(dbmd);
        } catch(SQLException se) {
            //Errores de JDBC
            se.printStackTrace();
        } catch(Exception e) {
            //Errores de Class.forName
            e.printStackTrace();
        } finally {
            try {
                if(conn!=null)
                    conn.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try  
    }
    
     public static void infoProducto(DatabaseMetaData dbmd) throws SQLException {
        System.out.println(">>>Informaci�n sobre el DBMS:");
        String producto=dbmd.getDatabaseProductName();
        String version=dbmd.getDatabaseProductVersion();
        boolean soportaSQL=dbmd.supportsANSI92EntryLevelSQL();
        boolean soportaConvert=dbmd.supportsConvert();
        boolean usaFich=dbmd.usesLocalFiles();
        boolean soportaGRBY=dbmd.supportsGroupBy();
        boolean soportaMinSQL=dbmd.supportsMinimumSQLGrammar();
        String nombre=dbmd.getUserName();
        String url=dbmd.getURL();
        System.out.println(" Producto: "+producto+" "+version);
        System.out.println(" Soporta el SQL ANSI92: "+soportaSQL);
        System.out.println(" Soporta la funci�n CONVERT entre tipos SQL: "+soportaConvert);
        System.out.println(" Almacena las tablas en ficheros locales: "+usaFich);
        System.out.println(" Nombre del usuario conectado: "+nombre);
        System.out.println(" URL de la Base de Datos: "+url);
        System.out.println(" Soporta GROUP BY: "+soportaGRBY);
        System.out.println(" Soporta la m�nima grm�tica SQL: "+soportaMinSQL);
        System.out.println();        
     }
     public static void infoDriver(DatabaseMetaData dbmd) throws SQLException {
        System.out.println(">>>Informaci�n sobre el driver:");
        String driver=dbmd.getDriverName();        
        String driverVersion=dbmd.getDriverVersion();
        int verMayor=dbmd.getDriverMajorVersion();
        int verMenor=dbmd.getDriverMinorVersion();
        System.out.println(" Driver: "+driver+" "+driverVersion);
        System.out.println(" Versi�n superior del driver: "+verMayor);
        System.out.println(" Versi�n inferior del driver: "+verMenor);
        System.out.println();         
     }
     public static void infoFunciones(DatabaseMetaData dbmd) throws SQLException {
        System.out.println(">>>Funciones del DBMS:");
        String funcionesCadenas=dbmd.getStringFunctions();
        String funcionesSistema=dbmd.getSystemFunctions();
        String funcionesTiempo=dbmd.getTimeDateFunctions();
        String funcionesNumericas=dbmd.getNumericFunctions();
        System.out.println(" Funciones de Cadenas: "+funcionesCadenas);
        System.out.println(" Funciones Num�ricas: "+funcionesNumericas);
        System.out.println(" Funciones del Sistema: "+funcionesSistema);
        System.out.println(" Funciones de Fecha y Hora: "+funcionesTiempo);
        System.out.println();
         
     }
     public static void infoTablas(DatabaseMetaData dbmd) throws SQLException {
        System.out.println(">>>Tablas existentes:");
        String patron="%";//listamos todas las tablas
        String tipos[]=new String[2];
        tipos[0]="TABLE";//tablas de usuario
        tipos[1]="SYSTEM TABLE";//tablas del sistema
        ResultSet tablas=dbmd.getTables(null,null,patron,tipos);
        boolean seguir=tablas.next();
        while(seguir){
            //Por cada tabla obtenemos su nombre y tipo
            System.out.println(" Nombre:"+tablas.getString("TABLE_NAME")+" Tipo:"+tablas.getString("TABLE_TYPE"));
            seguir=tablas.next();
        }
        System.out.println();        
         
     }
     public static void infoProcedimientos(DatabaseMetaData dbmd) throws SQLException {
        if(dbmd.supportsStoredProcedures()){
            System.out.println(">>>Procedimientos almacenados:");
            String patron="%";
            ResultSet procedimientos=dbmd.getProcedures(null,null,patron);
            boolean seguir=procedimientos.next();
            while(seguir){
                System.out.println(" "+procedimientos.getString("PROCEDURE_NAME"));
                seguir=procedimientos.next();
            }
        } else 
            System.out.println(">>>El DBMS no soporta procedimientos almacenados");
        System.out.println();                 
     }

}
