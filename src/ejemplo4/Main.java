/*
 * Main.java
 *
 * Created on 11 de agosto de 2008, 13:16
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo4;

/**
 *
 * @author pacoaldarias
 */

import java.sql.*;

public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
public static void main(String[] args) {
        // TODO code application logic here
        Connection conexion = null;
        Statement stmt = null;
        try {
            //Registrando el Driver
            String driver = "com.mysql.jdbc.Driver";
            
            registrar_driver;
            
            System.out.println("Driver "+driver+" Registrado correctamente");
            
            //Abrir la conexi�n con la Base de Datos por defecto
            System.out.println("Conectando con la Base de datos por defecto...");
            String jdbcUrl = "jdbc:mysql:///"; //Conexion como root para crear la instancia

            obtener_la_conexion_a_base_de_Datos_por_defecto;
            
            System.out.println("Conexi�n establecida con la Base de datos por defecto...");
           
            //Crear la base de datos prueba2
            crear_la_sentencia;
            
            String sql = "CREATE DATABASE prueba2"; //Creamos la base de datos
            
            ejecutar_la_sentencia;
            
            //Cerramos la conexi�n 
            stmt.close();
            conexion.close();
            

            //Abrir la conexi�n con la Base de Datos prueba2
            System.out.println("Conectando con la Base de datos prueba2...");
            jdbcUrl = "jdbc:mysql://localhost:3306/prueba2";
            
            obtener_una_conexion_a_Base_de_Datos_prueba2;
            
            System.out.println("Conexi�n establecida con la Base de datos prueba2...");


            crear_la_Sentencia;
            
            sql = "CREATE TABLE vendedores (" +
                    "id int NOT NULL auto_increment," +
                    "nombre varchar(50) NOT NULL default ''," +
                    "fecha_ingreso date NOT NULL default '0000-00-00'," +
                    "salario float NOT NULL default '0'," +
                    "PRIMARY KEY  (id))";
          
            ejecutar_la_Sentencia;
            
            
        } catch(SQLException se) {
            //Errores de JDBC
            se.printStackTrace();
        } catch(Exception e) {
            //Errores de Class.forName
            e.printStackTrace();
        } finally {
            try {
                if(stmt!=null)
                    stmt.close();                
                if(conexion!=null)
                    conexion.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try  
    }    
}
